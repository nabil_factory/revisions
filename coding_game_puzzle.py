def get_localisation_bloc(block_number, height, width, grid):
    localisation_block = []

    for x in range(height):
        for y in range(width):
            if grid[x][y] == block_number:

                localisation_block.append((x,y))
    return localisation_block


def get_direction_one_place(place, height, width, grid):
    place_x, place_y = place
    block_number = grid[place_x][place_y]

    left_row = grid[place_x][:place_y]
    LEFT = check_row_validity(block_number, left_row)

    if place_y < width - 1:
        right_row = grid[place_x][place_y+1:]
        RIGHT = check_row_validity(block_number, right_row)
    else:
        RIGHT = True

    up_row = [row[place_y] for row in grid[:place_x]]
    UP = check_row_validity(block_number, up_row)
    if place_x < height - 1 :
        down_row = [row[place_y] for row in grid[place_x + 1:]]
        DOWN = check_row_validity(block_number, down_row)
    else:
        DOWN = True
    return UP, DOWN, RIGHT, LEFT


def check_row_validity(block_number, row):
    validity = True
    for val in row:
        if val != block_number and val != ".":
            validity = False
            break
    return validity


def solve(width, height, nb_blocks, grid):
    for block_number in range(nb_blocks):
        block_number = str(block_number)
        localisations_block = get_localisation_bloc(block_number, height, width, grid)
        if localisations_block:
            UP, DOWN, RIGHT, LEFT = True, True, True, True
            for one_place in localisations_block:
                local_UP, local_DOWN, local_RIGHT, local_LEFT = get_direction_one_place(one_place, height, width, grid)
                UP = UP and local_UP
                DOWN = DOWN and local_DOWN
                RIGHT = RIGHT and local_RIGHT
                LEFT = LEFT and local_LEFT

            if UP:
                return "UP", block_number
            if DOWN:
                return "DOWN", block_number
            if RIGHT:
                return "RIGHT", block_number
            if LEFT:
                return "LEFT", block_number

    return -1


if __name__ == '__main__':
    # Logique du code :
    # Pour chaque nombre faire :
    # Chercher toutes les localisations de ce nombre
    # Pour chaque localisation faire :
    # Chercher pour les 4 directions si la voie est libre ou pas
    # Pour une direction, si la voie est libre pour toutes les localisations du nombre,
    # alors la voie est libre pour ce nombre.
    width = 5
    height = 4
    nb_blocks = 4
    grid = [
        "XX..X",
        "X22.X",
        ".2.3X",
        "X.33X"
    ]
    print(solve(width, height, nb_blocks, grid))