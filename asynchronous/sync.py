import os
import time
import asyncio
import aiohttp

api_key = os.getenv('ALPHAVANTAGE_API_KEY')
url = 'https://www.alphavantage.co/query?function=OVERVIEW&symbol={}&apikey={}'
symbols = ['AAPL', 'GOOG', 'TSLA', 'MSFT', 'AAPL']


def get_tasks(session):
    tasks = []
    for symbol in symbols:
        tasks.append(asyncio.create_task(session.get(url.format(symbol, api_key), ssl=False)))
    return tasks


async def get_symbols():
    response_tasks = []
    async with aiohttp.ClientSession() as session:
        
        for symbol in symbols:
            response_task = asyncio.create_task(session.get(url.format(symbol, api_key)))
            response_tasks.append(response_task)
        # response_tasks = get_tasks(session)
        responses = await asyncio.gather(*response_tasks)
    return responses


if __name__ == '__main__':
    start_time = time.time()
    responses = asyncio.run(get_symbols())
    results = []
    # for response in responses:
    #     results.append(response.json())
    total_time = time.time() - start_time
    print("Temps d'execution : ", total_time)